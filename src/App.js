import React from 'react';
import './App.css';
import 'antd/dist/antd.css';
// import bg from './img/pattern.jpg'
import { UserProvider } from './context/UserContext'
import Main from './layout/Main';

function App() {
  return (
    <>
      <UserProvider>
        <Main />
      </UserProvider>
    </>
  )
}

export default App;
