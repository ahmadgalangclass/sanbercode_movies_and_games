import React, { useContext } from "react"
import { UserContext } from './UserContext'
import { Alert } from 'antd';

const AlertNotif = () => {
    const [, , alert,] = useContext(UserContext)

    if (alert === "successregist") {
        return (
            <>
                <Alert
                    message="Success"
                    description="Registration Success."
                    type="success"
                    showIcon
                    closable
                />
            </>
        )
    }
    else if (alert === "failedregist") {
        return (
            <>
                <Alert
                    message="Error"
                    description="Registration Failed."
                    type="error"
                    showIcon
                    closable
                />
            </>
        )
    }
    else if (alert === "failedlogin") {
        return (
            <>
                <Alert
                    message="Error"
                    description="Login Failed."
                    type="error"
                    showIcon
                    closable
                />
            </>
        )
    }
    else if (alert === "successlogin") {
        return (
            <>
                <Alert
                    message="Success"
                    description="Login Success."
                    type="success"
                    showIcon
                    closable
                />
            </>
        )
    }
    else if (alert === "successadd") {
        return (
            <>
                <Alert
                    message="Success"
                    description="Add Data Success."
                    type="success"
                    showIcon
                    closable
                />
            </>
        )
    }
}

export default AlertNotif