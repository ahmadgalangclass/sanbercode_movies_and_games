import React, { useContext } from "react"
import { Link } from "react-router-dom";
import { UserContext } from "../context/UserContext";
import { Layout, Menu, Button, Avatar } from 'antd';

const Header = () => {
    const { Header } = Layout;

    const [user, setUser] = useContext(UserContext)

    const handleLogout = () => {
        setUser(null)
        localStorage.removeItem("user")
    }


    return (
        <>
            <Layout>
                <Header className="header" style={{ position: 'fixed', zIndex: 1, width: '100%' }}>
                    <div className="logo" />
                    <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['1']}>
                        <Menu.Item key="1"><Link to="/">Home</Link></Menu.Item>
                        <Menu.Item key="2"><Link to="/movie">Movies</Link></Menu.Item>
                        <Menu.Item key="3"><Link to="/game">Games</Link></Menu.Item>
                        {
                            user === null && (
                                <>
                                    <Menu.Item key="4" style={{ float: 'right' }}><Link to="/login">Login</Link></Menu.Item>
                                    <Menu.Item key="5" style={{ float: 'right' }}><Link to="/register">Register</Link></Menu.Item>
                                </>
                            )
                        }
                        {
                            user !== null && (
                                <>
                                    <Menu.Item key="6" style={{ float: 'right' }}>
                                        <Avatar size={40}>{user.name[0]} </Avatar>
                                    </Menu.Item>
                                    <Menu.Item key="7" style={{ float: 'right' }}>
                                        <Button onClick={handleLogout} >Logout</Button>
                                    </Menu.Item>

                                </>
                            )
                        }
                    </Menu>
                </Header>
            </Layout>,
        </>
    )
}

export default Header


