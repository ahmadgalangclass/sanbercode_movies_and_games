import React, { useContext, useState } from "react"
import {
    Switch,
    Route,
    Redirect,
    Link,
} from "react-router-dom";
import { Layout, Menu } from 'antd';
import { YoutubeOutlined, RobotOutlined } from '@ant-design/icons';
import Footer from './Footer'
import Login from '../pages/Login'
import Register from '../pages/Register'
import Home from '../pages/Home'
import Movies from '../pages/MovieList'
import Games from '../pages/GameList'
import SingleMovie from '../pages/SingleMovie'
import SingleGame from '../pages/SingleGame'
import MovieTable from '../pages/MovieTable'
import GameTable from '../pages/GameTable'
import CreateMovie from '../pages/CreateMovie'
import { UserContext } from '../context/UserContext'

const Content = () => {
    const [user] = useContext(UserContext);

    const { Content } = Layout;
    const { SubMenu } = Menu;
    const { Sider } = Layout;

    const PrivateRoute = ({ user, ...props }) => {
        if (user) {
            return <Route {...props} />;
        } else {
            return <Redirect to="/login" />;
        }
    };

    const LoginRoute = ({ user, ...props }) =>
        user ? <Redirect to="/" /> : <Route {...props} />;

    const [collapsed, setCollapsed] = useState(true)

    const onCollapse = collapsed => {
        console.log(collapsed);
        setCollapsed(collapsed)
    };


    return (
        <>
            <Layout>
                {
                    user !== null && (
                        <>
                            <Sider
                                className="site-layout-background"
                                collapsible collapsed={collapsed} onCollapse={onCollapse}>
                                <Menu
                                    mode="inline"
                                    defaultSelectedKeys={['1']}
                                    defaultOpenKeys={['sub1']}
                                    style={{ height: '100%', borderRight: 0, marginTop: '50px' }}
                                    theme="dark"
                                >
                                    <SubMenu key="sub2" icon={<YoutubeOutlined />} title="Movies">
                                        <Menu.Item key="5"><Link to="/movietable">Table</Link></Menu.Item>
                                        <Menu.Item key="6"><Link to="/createmovie">Add Movie</Link></Menu.Item>
                                    </SubMenu>
                                    <SubMenu key="sub3" icon={<RobotOutlined />} title="Games">
                                        <Menu.Item key="9"><Link to="/gametable">Table</Link></Menu.Item>
                                        <Menu.Item key="10">Add Data</Menu.Item>
                                    </SubMenu>
                                </Menu>
                            </Sider>
                        </>
                    )
                }
                <Content
                    className="site-layout-background"
                    style={{
                        padding: 24,
                        marginTop: 15,
                        minHeight: 380,
                        overflow: 'initial'
                    }}>
                    <section>
                        <Switch>
                            <Route exact path="/" user={user} component={Home} />
                            <Route exact path="/movie" user={user} component={Movies} />
                            <Route exact path="/game" user={user} component={Games} />
                            <Route exact path="/register" user={user} component={Register} />
                            <Route exact path="/singlemovie/:id" user={user} component={SingleMovie} />
                            <Route exact path="/singlegame/:id" user={user} component={SingleGame} />
                            <LoginRoute exact path="/login" user={user} component={Login} />
                            <PrivateRoute exact path="/movietable" user={user} component={MovieTable} />
                            <PrivateRoute exact path="/gametable" user={user} component={GameTable} />
                            <PrivateRoute exact path="/createmovie" user={user} component={CreateMovie} />
                        </Switch>
                    </section>
                </Content>
            </Layout>
            <Footer />
        </>
    )
}

export default Content