import React from "react"
import { Layout } from 'antd';

const Footer = () => {
    const { Footer } = Layout;

    return (
        <>
            <Footer style={{ color: 'white', textAlign: 'center', position: "fixed", bottom: 0, width: "100%", height: '20px', backgroundColor: "#000000" }}>Sanbercode ©2020 Created by Azam Fuadi</Footer>
        </>
    )
}

export default Footer