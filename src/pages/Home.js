import React, { useContext } from 'react'
import GameList from './GameList'
import MovieList from './MovieList'
import AlertNotif from '../context/Alert'
import { UserContext } from '../context/UserContext'

const Home = () => {
    const [, , alert,] = useContext(UserContext)
    return (
        <>
            {alert !== "" && <AlertNotif />}
            <MovieList />
            <GameList />
        </>
    )
}

export default Home